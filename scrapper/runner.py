import scrapy
from scrapy.selector import Selector
from scrapy.http import Request, FormRequest
from scrapy.utils.project import get_project_settings
from twisted.internet import reactor
from scrapy.crawler import CrawlerRunner, Crawler
from scrapy.utils.log import configure_logging

from scrapper.spiders.spiderBot import spiderBot

import re
from mysql.connector import MySQLConnection, Error
import time
import re
import sys

MySpider = spiderBot()
configure_logging({'LOG_FORMAT': '%(levelname)s: %(message)s'})
settings = get_project_settings()
runner = CrawlerRunner(settings)
runner.crawl(MySpider)
d = runner.join()
d.addBoth(lambda _: reactor.stop())
reactor.run()