# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy import Field

class ScrapperItems(scrapy.Item):

	product_link = Field()
	question = Field()
	author = Field()
	data_id = Field()
