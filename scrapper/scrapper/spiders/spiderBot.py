from scrapy import Spider
from scrapy.selector import Selector
from scrapy.http import Request, FormRequest
import re
from mysql.connector import MySQLConnection, Error
import time
import re
import sys
from scrapper.items import ScrapperItems
import sys
sys.path.append('./scrapper/database')
from db_config import read_db_config

class spiderBot(Spider):
    # """docstring for ."""
    def __init__(self,*args, **kwargs):
        self.remove_js = re.compile(r'http[s]?://(?:[a-z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-f][0-9a-f]))+')

    # user_agent ='Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'
    user_agent ='Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.100 Safari/537.36'
    name = "spiderBot"
    start_urls = ["https://www.lazada.co.id/ask/answer/new/?spm=a2o4j.other.0.0.587d4798FBjSSb&sku=CO698FAAA2UG0DANID&token=56e6e6f78328a4193fee0a5a1e94fe21ce503826"]

    def parse(self, response):
        url = "https://www.lazada.co.id/ask/answer/new/?spm=a2o4j.other.0.0.587d4798FBjSSb&sku=CO698FAAA2UG0DANID&token=56e6e6f78328a4193fee0a5a1e94fe21ce503826"
        yield Request(url=url, callback=self.list_questions,errback=self.errbackHttpbin)

    def list_questions(self,response):
        item = ScrapperItems()

        sel = Selector(response)
        questions = sel.css('.pending-questions')
        question_product = questions.css('.pending-questions__product')

        for question in question_product:
            product_link = question.css('.question-object__anchor::attr(href)').extract()[0]
            data_id = question.css('div.pending-question-item_inactive::attr(data-id)').extract()[0]
            # question_anchor = question.css('.pending-question-item__text::text')
            text = question.css('.pending-question-item__text::text').extract()[0]
            author = question.css('.pending-question-item__author::text').extract()[0]

            item['product_link'] = product_link
            item['question'] = text
            item['author'] = author
            item['data_id'] = data_id

            yield item

    def errbackHttpbin(self, failure):
        # log all failures
        self.logger.error(repr(failure))

        # in case you want to do something special for some errors,
        # you may need the failure's type:

        if failure.check(HttpError):
            # these exceptions come from HttpError spider middleware
            # you can get the non-200 response
            response = failure.value.response
            self.logger.error('HttpError on %s', response.url)

        elif failure.check(DNSLookupError):
            # this is the original request
            request = failure.request
            self.logger.error('DNSLookupError on %s', request.url)

        elif failure.check(TimeoutError, TCPTimedOutError):
            request = failure.request
            self.logger.error('TimeoutError on %s', request.url)
