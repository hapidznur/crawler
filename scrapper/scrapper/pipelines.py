# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

from mysql.connector import MySQLConnection, Error
import os.path
import sys
sys.path.append('./scrapper/database')
from db_config import read_db_config

from scrapy.xlib.pydispatch import dispatcher
from scrapy import signals
from scrapy.exceptions import DropItem

import json

class ScrapperPipeline(object):
    def process_item(self, item, spider):
        return item

class DuplicatesArticlePipeline(object):
    def __init__(self):
        self.duplicates = {}
        dispatcher.connect(self.spider_opened, signals.spider_opened)
        dispatcher.connect(self.spider_closed, signals.spider_closed)

    def spider_opened(self, spider):
        self.duplicates[spider] = set()

    def spider_closed(self, spider):
        del self.duplicates[spider]

    def process_item(self, item, spider):
        if item['data_id'] in self.duplicates[spider]:
            raise DropItem("Duplicate item found: %s" % item)
        else:
            self.duplicates[spider].add(item['data_id'])
            return item

class mysqlPipeline(object):
    """docstring for mysqlPipeline"""

    def open_spider(self, spider):
        db_config = read_db_config()
        self.client = MySQLConnection(**db_config)

    def process_item(self, item, spider):
        query = "INSERT INTO `questions`(`product_id`,`question`,`name_ask`,`product_link`) VALUES(%s,%s,%s,%s);"
        args= (item['data_id'],item['question'],item['author'],item['product_link'])

        try:
            cursor = self.client.cursor()
            cursor.execute(query, args)

            if cursor.lastrowid:
                print('last insert id', cursor.lastrowid)
            else:
                print('last insert id not found')

            self.client.commit()
        except Error as error:
            print(error)

    def close_spider(self, spider):
        self.client.close()
